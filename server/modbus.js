import {Meteor} from 'meteor/meteor';
import {Tracker} from 'meteor/tracker';
import {Logger} from 'meteor/ostrio:logger';
import {LoggerConsole} from 'meteor/ostrio:loggerconsole';
import {LoggerMongo} from 'meteor/ostrio:loggermongo';
// import Measurements from '../imports/api/measurements/collection';
import Settings from '../imports/api/settings/collection';
import Logs from '../imports/api/logs/collection';

const net = require('net');
const modbus = require('modbus-tcp');
const modbusServer = new modbus.Server();

let user = Meteor
    .users
    .findOne({roles: 'logger'});
let userId = user
    ? user._id
    : '';
const log = new Logger();

const writeRegister = Meteor.bindEnvironment((from, item, reply) => {
    log.info('Write register.', {
        from,
        item: item[1] + ' ' + item[0]
    }, userId);
    reply();
});

const ToUInt = (item) => {
    if (item > 47)
        return item - 48;
    else
        return 0;
    }
;

const writeRegisters = Meteor.bindEnvironment((from, to, items, reply) => {
    // items.forEach(item => rez += ' ' + item[1] + ' ' + item[0])
    // modbus.transactionId);

    const record = {};
    if (from == 146) {
        // Дата время
        var year = +items[0][0] * 256 + items[0][1];
        var month = +items[1][0] * 256 + items[1][1] - 1;
        var date = +items[2][0] * 256 + items[2][1];
        var hours = +items[4][0] * 256 + items[4][1];
        var minutes = +items[5][0] * 256 + items[5][1];
        var seconds = +items[6][0] * 256 + items[6][1];

        var date = new Date(year, month, date, hours, minutes, seconds, 0);

        record.recordDate = date;

        // Номер платформы
        var Pres = String(((items[8][0] * 256 + items[8][1]) * 256 + items[7][0]) * 256 + items[7][1]);
        Pres += '-';
        Pres += String(items[9][0] * 256 + items[9][1]);
        record.controlId = Pres;

        // РФ метка
        let res = '';

        // 	for (var i = 0; i < 9; i++) { 		if((i & 1) == 1) 			res +=
        // String.fromCharCode(items[10+i/2][0]); 		else 			res +=
        // String.fromCharCode(items[10][1]) + '-' + String.fromCharCode(items[10][0]));

        res += String.fromCharCode(items[10][1]);
        res += String.fromCharCode(items[10][0]);
        res += String.fromCharCode(items[11][1]);
        res += String.fromCharCode(items[11][0]);
        res += String.fromCharCode(items[12][1]);
        res += String.fromCharCode(items[12][0]);
        res += String.fromCharCode(items[13][1]);
        res += String.fromCharCode(items[13][0]);
        res += String.fromCharCode(items[14][1]);
        res += String.fromCharCode(items[14][0]);
        res += String.fromCharCode(items[15][1]);

        record.label = res;
        // Вес 	var buf = new Buffer(2); 	buf.writeUInt16BE(items[16], 0); 	var weight =
        // buf;
        var weight = +items[16][0] * 256 + items[16][1];
        record.weight = weight;

        record.toExport = false;

        Meteor.call('measurements.upsert', record, (err, res) => {
            if (err) {
                log.error('Record Insert Error', err, userId);
            }

        })

        reply();
    }

});

const writeCoil = Meteor.bindEnvironment(function (from, item, reply) {
    log.info('Write ciol.', {
        from,
        item: item[0]
    }, userId);
    reply();
});

const writeCoils = Meteor.bindEnvironment(function (from, to, items, reply) {
    let res = ''
    items.forEach(item => res += ' ' + item)
    log.info('Write coils.', {
        from,
        to,
        items: res
    }, userId);

    reply();
});

Meteor.startup(() => {
    user = Meteor
        .users
        .findOne({roles: 'logger'});
    userId = user
        ? user._id
        : '';

    const portSetting = Settings.findOne({code: 'TCP_PORT'});
    const port = portSetting && parseInt(portSetting.value) > 0 && parseInt(portSetting.value) < 65536
    ? portSetting.value
    : '57787';

    new LoggerMongo(log, {collection: Logs}).enable();
    if (Meteor.isDevelopment) {
        new LoggerConsole(log).enable();
    }

    const tcpServer = net.createServer();

    tcpServer.listen(port, Meteor.bindEnvironment(() => {
        log.info('TCP Socket bound', {
            port
        }, userId);
    }));

    const restartServer = Meteor.bindEnvironment((port, log) => {
        const oldPort = tcpServer
            .address()
            .port;
        const tcpServer2 = tcpServer;
        tcpServer.close(Meteor.bindEnvironment(() => {
            console.log('Server was stopped', { port: oldPort });
            tcpServer2.listen(port, Meteor.bindEnvironment(() => {
                console.log('TCP Socket restarted to another port', { port });
            }));
        }));
    });

    Settings
    .find()
        .observe({
            changed: (newSetting, oldSetting) => {
                if (newSetting.code == 'TCP_PORT') {
                    const newPort = parseInt(newSetting.value);
                    const oldPort = tcpServer.address() ? parseInt(tcpServer.address().port) : false;
                    // delete Config[oldSetting.key]; Config[newSetting.key] = newSetting.value;
                    if (newPort != oldPort) {
                        console.log('before restart', newPort, log);
                        log.info('TCP Socket restarted to another port', { port }, userId);
                        restartServer(newPort);
                    }
                }
            }
        });

    tcpServer.on('connection', Meteor.bindEnvironment((socket) => {
        log.info('client has connected', {}, userId);
        modbusServer.pipe(socket);

        socket.on('error', Meteor.bindEnvironment((e) => {
            log.error('Connection error', {
                e
            }, userId);
            socket.destroy();
        }));

        socket.on('close', Meteor.bindEnvironment((e) => {
            log.info('Client has closed connection.', {}, userId);
        }));
    }));

    modbusServer.on('write-single-register', writeRegister);
    modbusServer.on('write-multiple-registers', writeRegisters);
    modbusServer.on('write-single-coil', writeCoil);
    modbusServer.on('write-multiple-coils', writeCoils);
});
