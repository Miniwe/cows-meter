import { Meteor } from 'meteor/meteor';
import { TAPi18n } from 'meteor/tap:i18n';
import { Bert } from 'meteor/themeteorchef:bert';
import { moment } from 'meteor/momentjs:moment';
import { $ } from 'meteor/jquery';
import dataTablesBootstrap from 'datatables.net-bs4';
import dataTableResponsive from 'datatables.net-responsive';
import 'datatables.net-bs4/css/dataTables.bootstrap4.css';
// import dataTableRowGroup from 'datatables.net-rowgroup';

import '../../lib/accounts/accounts';
import '../../lib/helpers';
import '../../lib/routes';

const getUserLanguage = function () {
  return (Meteor.user() && Meteor.user().language) ? Meteor.user().language : 'ru';
};


Bert.defaults.style = 'growl-bottom-right';

Meteor.startup(() => {
  dataTablesBootstrap(window, $);
  dataTableResponsive(window, $);
  // dataTableRowGroup(window, $);
  const userLang = getUserLanguage();
  moment.locale(userLang);

  Session.set('showLoadingIndicator', true);

  TAPi18n.setLanguage(userLang)
    .done(() => {
      Session.set('showLoadingIndicator', false);
    })
    .fail((error_message) => {
      // Handle the situation
      console.log(error_message);
    });

});
