import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { moment } from 'meteor/momentjs:moment';
import uniq from 'lodash/uniq';
import map from 'lodash/map';
import Measurements from '../../api/measurements/collection';
import Users from '../../api/users/collection';
import rateLimit from '../../modules/rate-limit.js';

export const tmpMethod = new ValidatedMethod({
    name: 'tmp',
    validate() {},
    run() {
        throw new Meteor.Error('tmp', 'Test Call');
    },
});

export const getStatistics = new ValidatedMethod({
    name: 'global.getStatistics',
    validate() {},
    run() {
        const res = {};
        const measurements = Measurements.find();
        const users = Users.find();
        res.animalsCount = uniq(map(measurements.fetch(), item => item.label)).length;
        res.usersCount = users.count();
        res.measurementsCount = measurements.count();
        res.animalsDate = moment(new Date()).calendar();
        res.usersDate = moment(new Date()).calendar();
        res.measurementsDate = moment(new Date()).calendar();
        return res;
    },
});

export const setuserdata = new ValidatedMethod({
    name: 'setuserdata',
    validate({ userId, key, value }) {
        check(userId, String);
        check(key, String);
    },
    run({ userId, key, value }) {
        let set = {};
        set[key] = value;
        Meteor.users.update(userId, { $set: set });
    },
});

rateLimit({
    methods: [ tmpMethod, setuserdata, getStatistics ],
    limit: 5,
    timeRange: 1000,
});
