import { ValidatedMethod } from 'meteor/mdg:validated-method';
import rateLimit from '../../modules/rate-limit.js';

import measurementsFixtures from '../../api/measurements/fixtures';
import usersFixtures from '../../api/users/fixtures';
import settingsFixtures from '../../api/settings/fixtures';

Meteor.startup(() => {
  measurementsFixtures(30);
  settingsFixtures();
  usersFixtures();
});


const resetTable = new ValidatedMethod({
  name: 'resetTable',
  validate({ table, count }) {
    check(table, String);
    check(count, Number);
  },
  run({ table, count }) {
    switch (table) {
      case 'measurements':
        measurementsFixtures(count);
        break;
      case 'settings':
        settingsFixtures(count);
        break;
      case 'users':
        usersFixtures(count);
        break;
      default:
        throw new Meteor.Error('resetTable', 'Not defined table');
    }
  }
});

rateLimit({
  methods: [
    resetTable,
  ],
  limit: 5,
  timeRange: 1000
});
