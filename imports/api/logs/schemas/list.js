import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

const ListSchema = new SimpleSchema({
  label: {
    type: String,
    label: 'Label',
  },
  value: {
    type: String,
    label: 'Value',
  }
});

export default ListSchema;
