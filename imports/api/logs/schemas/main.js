import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions([ 'autoform' ]);

const MainSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
    label: 'Id',
    autoform: {
      afFieldInput: {
        type: "hidden"
      },
      afFormGroup: {
        label: false
      }
    }
  },
  userId: {
    type: String,
    label: 'UserId'
  },
  date: {
    type: Date,
    label: 'Date',
  },
  timestamp: {
    type: String,
    label: 'Timestamp',
  },
  level: {
    type: String,
    label: 'Level',
  },
  message: {
    type: String,
    label: 'Message',
  },
  additional: {
    type: Object,
    label: 'Details',
    blackbox: true
  }
});

export default MainSchema;
