/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Factory } from 'meteor/dburles:factory';
import Logs from './collection';
import { upsertLog, removeLog } from './methods';

describe('Logs methods', function () {
  beforeEach(function () {
    if (Meteor.isServer) {
      resetDatabase();
    }
  });

  it('inserts a log into the Logs collection', function () {
    upsertLog.call({title: 'You can\'t arrest me, I\'m the Cake Boss!', body: 'They went nuts!'});

    const getLog = Logs.findOne({title: 'You can\'t arrest me, I\'m the Cake Boss!'});
    assert.equal(getLog.body, 'They went nuts!');
  });

  it('updates a log in the Logs collection', function () {
    const { _id } = Factory.create('log');

    upsertLog.call({ _id, title: 'You can\'t arrest me, I\'m the Cake Boss!', body: 'They went nuts!'});

    const getLog = Logs.findOne(_id);
    assert.equal(getLog.title, 'You can\'t arrest me, I\'m the Cake Boss!');
  });

  it('removes a log from the Logs collection', function () {
    const { _id } = Factory.create('log');
    removeLog.call({ _id });
    const getLog = Logs.findOne(_id);
    assert.equal(getLog, undefined);
  });
});
