import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import Collection from './collection';

const logsOptions = {
  name: 'Logs',
  collection: Collection,
  columns: [
    { data: 'date', title: 'Date' },
    { data: 'level', title: 'Level' },
    { data: 'message', title: 'Message' },
    {
      tmpl: Meteor.isClient && Template.logsCell
    }
  ],
  sDom: 'ft<"row"<"col-3"l><"col-2"i><"col-7"p>>',
  responsive: true,
  // dataSrc: 'group',
  autoWidth: !(Meteor.isClient && window.screen.availWidth < 480),
  changeSelector(selector) {
    return selector;
  }
};

const lTabular = new Tabular.Table(logsOptions);
