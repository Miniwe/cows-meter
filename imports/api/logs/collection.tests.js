/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { assert } from 'meteor/practicalmeteor:chai';
import Logs from './collection';

describe('Logs collection', function () {
  it('registers the collection with Mongo properly', function () {
    assert.equal(typeof Logs, 'object');
  });
});
