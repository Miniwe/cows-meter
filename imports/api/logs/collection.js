import { Mongo } from 'meteor/mongo';
import extend from 'lodash/extend';
import MainSchema from './schemas/main';
// import ListSchema from './schemas/list';
// import ViewSchema from './schemas/view';

const Logs = new Mongo.Collection('ostrioMongoLogger');

extend(Logs, {
  ids() {
    return this.find({}, { fields: { _id: 1 } }).fetch().map((item) => item._id);
  },
  getSchema(name) {
    let schema;
    switch (name) {
      case 'list':
        schema = ListSchema;
        break;
      case 'view':
        schema = ViewSchema;
        break;
      default:
        schema = MainSchema;
    }
    return schema;
  },
  list() {
    return this.find();
  },
});

Logs.allow({
  insert : () => false,
  update : () => false,
  remove : () => false,
});

Logs.deny({
  insert : () => true,
  update : () => true,
  remove : () => true,
});

Logs.attachSchema(MainSchema);
// Logs.attachSchema(MainSchema, { selector: { type: 'main' } });
// Logs.attachSchema(ListSchema, { selector: { type: 'list' } });
// Logs.attachSchema(ViewSchema, { selector: { type: 'view' } });

export default Logs;
