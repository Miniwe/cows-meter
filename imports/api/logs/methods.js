// import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Logs from './collection';
import rateLimit from '../../modules/rate-limit.js';
import MainSchema from './schemas/main';

export const upsertLog = new ValidatedMethod({
  name: 'logs.upsert',
  // validate: MainSchema.validator(),
  validate(data) {
    check(data, Object);
  },
  run(log) {
    return Logs.upsert({
      _id: log._id
    }, { $set: log }); // , { selector: { type: 'main' } }
  }
});

export const removeLog = new ValidatedMethod({
  name: 'logs.remove',
  validate(id) {
    check(id, String);
  },
  run(id) {
    if (Logs.remove(id)) {
      return 'record_removed';
    }
    else {
      throw new Meteor.Error('logs', 'record_remove_error');
    }
  }
});

rateLimit({
  methods: [
    upsertLog, removeLog
  ],
  limit: 5,
  timeRange: 1000
});
