import Users from '../collection';

Meteor.publish('users.list', (query = {}, params = {}) => {
  check(query, Object);
  check(params, Object);
  return Users.list(query, params);
});

Meteor.publish('users.single', (id) => {
  check(id, Match.Maybe(String, null));
  return [
    Users.find(id, {fields: {
      'username': 1,
      'language': 1,
      'profile': 1,
      'emails': 1,
      'roles': 1
    }})
  ];
});
