import { Roles } from 'meteor/alanning:roles';
import extend from 'lodash/extend';
import MainSchema from './schemas/main';
import ListSchema from './schemas/list';
import ViewSchema from './schemas/view';
import ProfileSchema from './schemas/profile';
import User from './model';

const Users = Meteor.users;

extend(Users, {
  _transform(item) {
    return new User(item);
  },
  ids() {
    return this.find({}, { fields: { _id: 1 } }).fetch().map((item) => item._id);
  },
  getSchema(name) {
    let schema;
    switch (name) {
      case 'profile':
        schema = ProfileSchema;
        break;
      case 'list':
        schema = ListSchema;
        break;
      case 'view':
        schema = ViewSchema;
        break;
      default:
        schema = MainSchema;
    }
    return schema;
  },
  list(query, params) {
    return this.find(query, params);
  }
});

Users.allow({
  insert : () => false,
  update : () => false,
  remove : () => false,
});

Users.deny({
  insert : () => true,
  update : () => true,
  remove : () => true,
});

Users.after.insert((userId, { _id }) => {
  Roles.addUsersToRoles(_id, [ 'user' ]);
});

export default Users;
