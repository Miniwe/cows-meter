// import SimpleSchema from 'simpl-schema';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Users from './collection';
import rateLimit from '../../modules/rate-limit.js';
import MainSchema from './schemas/main';
import ProfileSchema from './schemas/profile';

export const upsertUser = new ValidatedMethod({
  name: 'users.upsert',
  validate: (data) => {
    MainSchema.validator({ clean: true });
  },
  run(user) {
    if (user._id) {
      const emails = user.emails;
      delete user.emails; /** @todo correct update emails after */
      if (user.password) {
        Accounts.setPassword(user._id, user.password);
      } else {
        delete user.password;
      }
      Roles.addUsersToRoles(user._id, user.roles);
      return Users.upsert(
        {
          _id: user._id,
        },
        {
          $set: user,
        }
      );
    } else {
      throw new Meteor.Error('users', 'User must register self first');
    }
  },
});

export const profileUpdate = new ValidatedMethod({
  name: 'profile.update',
  validate: ProfileSchema.validator(),
  run(user) {
    if (user._id) {
      if (user.password) {
        Accounts.setPassword(user._id, user.password, { logout: false });
      } else {
        delete user.password;
      }
      return Users.upsert(
        {
          _id: user._id,
        },
        {
          $set: user
        }
      );
    } else {
      throw new Meteor.Error('users', 'Update profile failed');
    }
  },
});

export const removeUser = new ValidatedMethod({
  name: 'users.remove',
  validate(id) {
    check(id, String);
  },
  run(id) {
    return Users.remove(id);
  },
});

rateLimit({
  methods: [ upsertUser, removeUser, profileUpdate ],
  limit: 5,
  timeRange: 1000,
});
