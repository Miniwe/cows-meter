import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';

const fixtures = (count = 1) => {
  if (count === 0) {
    Meteor.users.remove({});
  } else if (!Meteor.users.find().count()) {
    const users = [ {
      email: 'admin@cows.co',
      language: 'ru',
      password: 'admin',
      username: 'admin',
      roles: [ 'admin', 'user' ],
    }, {
      email: 'logger@cows.co',
      language: 'en',
      password: 'logger123456',
      username: 'logger [do not remove role `logger`]',
      roles: ['logger'],
    } ];

    users.forEach(({ email, password, username, language, roles }) => {
      const userExists = Meteor.users.findOne({ 'emails.address': email });

      if (!userExists) {
        const userId = Accounts.createUser({ email, password, username, language });
        Roles.addUsersToRoles(userId, roles);
      }
    });
  }
};

export default fixtures;
