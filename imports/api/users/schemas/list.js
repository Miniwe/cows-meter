import SimpleSchema from 'simpl-schema';

const ListSchema = new SimpleSchema({
  username: {
    type: String,
    label: 'Username',
  },
  roles: {
    type: Array,
    label: 'Roles',
  },
  'roles.$': {
    type: String,
    label: 'Role',
  },
  emails: {
    type: Array,
    label: 'Emails',
  },
  'emails.$': {
    type: Object,
    label: 'Email',
  },
  createdAt: {
    type: String,
    label: 'Created At'
  },
});

export default ListSchema;
