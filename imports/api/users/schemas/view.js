import SimpleSchema from 'simpl-schema';

const ViewSchema = new SimpleSchema({
  username: {
    type: String,
    label: 'Username',
  },
  roles: {
    type: Array,
    label: 'Roles',
  },
  'roles.$': {
    type: String,
    label: 'Role',
  },
  emails: {
    type: Array,
    label: 'Emails',
  },
  'emails.$': {
    type: Object,
    label: 'Email',
  }
});

export default ViewSchema;

