import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import Collection from './collection';

const usersOptions = {
  name: 'Users',
  collection: Collection,
  columns: [
    { data: 'username', title: 'Username' },
    { data: 'emails.0.address', title: 'Email' },
    {
      data: 'createdAt',
      title: 'Created Date',
      render: function (val, type, doc) {
        if (val instanceof Date) {
          return moment(new Date(val)).format('MMMM Do YYYY, h:mm');
        }
        return 'Never';
      }
    },
    {
      tmpl: Meteor.isClient && Template.usersCell
    }
  ],
  sDom: 'ft<"row"<"col-3"l><"col-2"i><"col-7"p>>',
  responsive: true,
  // dataSrc: 'group',
  autoWidth: !(Meteor.isClient && window.screen.availWidth < 480),
  changeSelector(selector) {
    return selector;
  }
};

const uTabular = new Tabular.Table(usersOptions);
