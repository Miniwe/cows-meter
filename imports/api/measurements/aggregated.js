import { Mongo } from 'meteor/mongo';
import extend from 'lodash/extend';
// import MainSchema from './schemas/main';
import AggregatedSchema from './schemas/aggregated';
// import ListSchema from './schemas/list';
// import ViewSchema from './schemas/view';

const MeasurementsAggregated = new Mongo.Collection('Measurements.Aggredated');

extend(MeasurementsAggregated, {
  ids() {
    return this.find({}, { fields: { _id: 1 } }).fetch().map((item) => item._id);
  },
  getSchema(name) {
    let schema;
    switch (name) {
      case 'list':
        // schema = ListSchema;
        break;
      case 'view':
        // schema = ViewSchema;
        break;
      default:
        schema = AggregatedSchema;
    }
    return schema;
  },
  list() {
    return this.find();
  },
});

MeasurementsAggregated.allow({
  insert : () => false,
  update : () => false,
  remove : () => false,
});

MeasurementsAggregated.deny({
  insert : () => true,
  update : () => true,
  remove : () => true,
});

MeasurementsAggregated.attachSchema(AggregatedSchema);
// MeasurementsAggregated.attachSchema(MainSchema, { selector: { type: 'main' } });
// MeasurementsAggregated.attachSchema(ListSchema, { selector: { type: 'list' } });
// MeasurementsAggregated.attachSchema(ViewSchema, { selector: { type: 'view' } });

export default MeasurementsAggregated;
