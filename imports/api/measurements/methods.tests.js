/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Factory } from 'meteor/dburles:factory';
import Measurements from './collection';
import { upsertMeasurement, removeMeasurement } from './methods';

describe('Measurements methods', function () {
  beforeEach(function () {
    if (Meteor.isServer) {
      resetDatabase();
    }
  });

  it('inserts a measurement into the Measurements collection', function () {
    upsertMeasurement.call({title: 'You can\'t arrest me, I\'m the Cake Boss!', body: 'They went nuts!'});

    const getMeasurement = Measurements.findOne({title: 'You can\'t arrest me, I\'m the Cake Boss!'});
    assert.equal(getMeasurement.body, 'They went nuts!');
  });

  it('updates a measurement in the Measurements collection', function () {
    const { _id } = Factory.create('measurement');

    upsertMeasurement.call({ _id, title: 'You can\'t arrest me, I\'m the Cake Boss!', body: 'They went nuts!'});

    const getMeasurement = Measurements.findOne(_id);
    assert.equal(getMeasurement.title, 'You can\'t arrest me, I\'m the Cake Boss!');
  });

  it('removes a measurement from the Measurements collection', function () {
    const { _id } = Factory.create('measurement');
    removeMeasurement.call({ _id });
    const getMeasurement = Measurements.findOne(_id);
    assert.equal(getMeasurement, undefined);
  });
});
