import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import MeasurementsAggregated from '../collection';

Meteor.publish('aggregated.list', (query = {}, params = {}) => {
  check(query, Object);
  check(params, Object);
  return MeasurementsAggregated.find(query, params);
});

Meteor.publish('aggregated.single', (id) => {
  check(id, Match.Maybe(String, null));
  return MeasurementsAggregated.find(id);
});
