import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Measurements from '../collection';

Meteor.publish('measurements.list', (query = {}, params = {}) => {
  check(query, Object);
  check(params, Object);
  return Measurements.find(query, params);
});

Meteor.publish('measurements.single', (id) => {
  check(id, Match.Maybe(String, null));
  return Measurements.find(id);
});
