/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { assert } from 'meteor/practicalmeteor:chai';
import Measurements from './collection';

describe('Measurements collection', function () {
  it('registers the collection with Mongo properly', function () {
    assert.equal(typeof Measurements, 'object');
  });
});
