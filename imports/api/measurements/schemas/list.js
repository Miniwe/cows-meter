import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

const ListSchema = new SimpleSchema({
  icon: {
    type: String,
    optional: true
  },
  name: {
    type: String,
    label: 'Name',
  }
});

export default ListSchema;
