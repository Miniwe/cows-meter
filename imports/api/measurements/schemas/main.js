import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

const MainSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        optional: true,
        label: 'Id',
        autoform: {
            afFieldInput: {
                type: 'hidden'
            },
            afFormGroup: {
                label: false
            }
        }
    },
    recordDate: {
        type: Date,
        label: 'Record Date',
        autoform: {
            afFieldInput: {
                type: 'bootstrap-datetimepicker',
                dateTimePickerOptions: {
                    sideBySide: true,
                    showClose: true,
                    icons: {
                        time: 'fa fa-time',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-screenshot',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    }
                }
            }
        }
    },
    label: {
        type: String,
        label: 'Label'
    },
    weight: {
        type: Number,
        label: 'Weight'
    },
    controlId: {
        type: String,
        label: 'Control Id'
    },
    controlSum: {
        type: Number,
        label: 'Control Sum',
        optional: true
    },
    toExport: {
        type: Boolean,
        defaultValue: true,
        label: 'To Export'
    }
});

export default MainSchema;
