import SimpleSchema from 'simpl-schema';
import MainSchema from './main';

SimpleSchema.extendOptions([ 'autoform' ]);

const AggregatedSchema = MainSchema.omit('controlId', 'controlSum', 'toExport');

export default AggregatedSchema;
