import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

const ViewSchema = new SimpleSchema({
  _id: {
    type: String,
    label: 'Id'
  },
  name: {
    type: String,
    label: 'Name'
  },
  icon: {
    type: String,
    optional: true
  }
});

export default ViewSchema;
