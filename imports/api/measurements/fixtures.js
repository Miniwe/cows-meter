import { faker } from 'meteor/practicalmeteor:faker';
import times from 'lodash/times';
import Measurements from './collection';

const makeFakeData = () => {
  let label;
  if (Math.random() > 0.20) {
    const element = faker.random.arrayElement(Measurements.find().fetch());
    if (element) {
      label = element.label;
    } else {
      label = faker.internet.password(11) + '0';
    }
  } else {
    label = faker.internet.password(11) + '0';
  }
  const item = {
    recordDate: faker.date.past(6),
    label: label,
    weight: faker.random.number({ min: 100, max: 1500 }),
    controlId: faker.random.number(),
    controlSum: faker.random.number({ min: 2000, max: 3000 }),
    toExport: faker.random.boolean()
  };
  return item;
};

const fixtures = (count = 15) => {
  if (count === 0) {
    Measurements.remove({});
  } else if (!Measurements.find().count()) {
    times(count, () => {
      Measurements.insert(makeFakeData(), {selector: {type: 'main'}});
    });
  }
};

export default fixtures;
