import Tabular from 'meteor/aldeed:tabular';
import extend from 'lodash/extend';
import { Template } from 'meteor/templating';
import moment from 'moment';
import Collection from './collection';
import AggregatedCollection from './aggregated';
import { formatWeight } from '../../lib/helpers';

const measurementsOptions = {
    name: 'Measurements',
    collection: Collection,
    order: [ 1, 'desc' ],
    columns: [
        { data: 'label', title: 'Label' },
        {
            data: 'recordDate',
            title: 'Record Date',
            render(val, type, doc) {
                if (val instanceof Date) {
                    return moment(new Date(val)).format('MMMM Do YYYY, h:mm');
                }
                return 'Never';
            },
        },
        {
            data: 'weight',
            title: 'Weight',
            render(val, type, doc) {
                return formatWeight(val);
            },
        },
        {
            data: 'toExport',
            title: 'To Export',
            render(val, type, doc) {
                return val ? 'Yes' : 'No';
            },
        },
        {
            tmpl: Meteor.isClient && Template.measurementCell,
        },
    ],
    sDom: 't<"row"<"col-3"l><"col-2"i><"col-7"p>>',
    responsive: true,
    // dataSrc: 'group',
    autoWidth: !(Meteor.isClient && window.screen.availWidth < 480),
    changeSelector(selector) {
        // console.log('changeSelector', selector);
        return selector;
    },
};

const mTabular = new Tabular.Table({
    ...measurementsOptions,
});

const mTabularAggregated = new Tabular.Table(
    extend(measurementsOptions, {
        name: 'MeasurementsAggregated',
        collection: AggregatedCollection,
        columns: [
            {
                data: 'label',
                title: 'Label',
            },
            {
                data: 'recordDate',
                title: 'Last Update',
                render(val, type, doc) {
                    if (val instanceof Date) {
                        return moment(new Date(val)).format('MMMM Do YYYY, h:mm');
                    }
                    return 'Never';
                },
            },
            {
                data: 'weight',
                title: 'Average Weight',
                render(val, type, doc) {
                    return formatWeight(val);
                },
            },
        ],
    }),
);
