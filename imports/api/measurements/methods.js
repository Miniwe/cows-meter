// import SimpleSchema from 'simpl-schema';
import {ValidatedMethod} from 'meteor/mdg:validated-method';
import {moment} from 'meteor/momentjs:moment';
import map from 'lodash/map';
import uniq from 'lodash/uniq';
import slice from 'lodash/slice';
import Measurements from './collection';
import rateLimit from '../../modules/rate-limit.js';
import MainSchema from './schemas/main';

export const upsertMeasurement = new ValidatedMethod({
    name: 'measurements.upsert',
    validate: MainSchema.validator(),
    run(measurement) {
        return Measurements.upsert({
            _id: measurement._id
        }, {$set: measurement}); // , { selector: { type: 'main' } }
    }
});

export const removeMeasurement = new ValidatedMethod({
    name: 'measurements.remove',
    validate(id) {
        check(id, String);
    },
    run(id) {
        if (Measurements.remove(id)) {
            return 'record_removed';
        } else {
            throw new Meteor.Error('measurements', 'record_remove_error');
        }
    }
});

export const labelsList = new ValidatedMethod({
    name: 'measurements.labels',
    validate({term, _type}) {
        check(term, String);
        check(_type, 'query');
    },
    run({term}) {
        this.unblock();
        const query = {
            label: {
                $regex: `.*${term}.*`
            }
        };
        const params = {
            fields: {
                label: 1
            }
        };
        const results = Measurements
            .find(query, params)
            .fetch();
        return uniq(map(results, item => item.label));
    }
});

export const animalsList = new ValidatedMethod({
    name: 'animals.list',
    validate({query, params}) {
        check(query, Object);
        check(params, Object);
    },
    run({query, params}) {
        this.unblock();
        if (query.latest) {
            if (query.recordDate && query.recordDate.$lt) {
                query.recordDate = {
                    ...query.recordDate,
                    $lt: moment
                        .utc(new Date(query.recordDate.$lt))
                        .endOf('day')
                        .toDate()
                }

            } else {
                query.recordDate = {
                    $gt: moment()
                        .startOf('day')
                        .toDate(),
                    $lt: moment()
                        .endOf('day')
                        .toDate()
                }
            }
            delete query.latest;
        }
        const limit = params.limit || 10;
        const page = params.page || 1;
        params = params || {};
        delete params.limit;
        delete params.page;
        params.sort = {
            recordDate: -1
        };
        const results = Measurements
            .find(query, params)
            .fetch();
        let table = uniq(map(results, item => item.label));
        let firstWeight = 0;
        let lastWeight = 0;
        table = map(table, (item, index) => {
            const minValues = Measurements.findOne({
                label: item
            }, {
                sort: {
                    recordDate: 1
                }
            });
            const maxValues = Measurements.findOne({
                label: item
            }, {
                sort: {
                    recordDate: -1
                }
            });
            firstWeight += parseInt(minValues.weight, 10);
            lastWeight += parseInt(maxValues.weight, 10);
            return {
                label: item,
                firstDate: moment(new Date(minValues.recordDate)).format('MMMM Do YYYY, h:mm'),
                firstWeight: minValues.weight,
                lastDate: moment(new Date(maxValues.recordDate)).format('MMMM Do YYYY, h:mm'),
                lastWeight: maxValues.weight,
                increased: parseInt(maxValues.weight, 10) - parseInt(minValues.weight, 10)
            };
        });

        return {
            totals: {
                records: table.length,
                first_data: 'Start Weight',
                first_weigth: firstWeight,
                last_data: 'Finish Weight',
                last_weigth: lastWeight,
                increased: lastWeight - firstWeight
            },
            count: table.length,
            table: slice(table, (page - 1) * limit, page * limit)
        };
        // return Measurements.find().fetch();
    }
});

rateLimit({
    methods: [
        upsertMeasurement, removeMeasurement
    ],
    limit: 5,
    timeRange: 1000
});
