import { Mongo } from 'meteor/mongo';
import extend from 'lodash/extend';
import reduce from 'lodash/reduce';
import MainSchema from './schemas/main';
import MeasurementsAggregadet from './aggregated';
// import ListSchema from './schemas/list';
// import ViewSchema from './schemas/view';

const Measurements = new Mongo.Collection('Measurements');

extend(Measurements, {
    ids() {
        return this.find({}, { fields: { _id: 1 } }).fetch().map((item) => item._id);
    },
    getSchema(name) {
        let schema;
        switch (name) {
            case 'list':
                schema = ListSchema;
                break;
            case 'view':
                schema = ViewSchema;
                break;
            default:
                schema = MainSchema;
        }
        return schema;
    },
    list() {
        return this.find();
    },
});

Measurements.allow({
    insert: () => false,
    update: () => false,
    remove: () => false,
});

Measurements.deny({
    insert: () => true,
    update: () => true,
    remove: () => true,
});

Measurements.attachSchema(MainSchema);
// Measurements.attachSchema(MainSchema, { selector: { type: 'main' } });
// Measurements.attachSchema(ListSchema, { selector: { type: 'list' } });
// Measurements.attachSchema(ViewSchema, { selector: { type: 'view' } });

const updateAggregadet = ({ label, recordDate, weight }) => {
    let aggRecord = MeasurementsAggregadet.findOne({ label });
    if (aggRecord) {
        const records = Measurements.find({ label }).fetch();
        const fEl = records.shift();
        let aggObject = {
            aggDate: fEl.recordDate,
            aggWeight: fEl.weight,
            aggWeight: fEl.weight,
        };
        aggObject = reduce(
            records,
            (aggObject, record) => {
                const newAggObject = {
                    aggDate: aggObject.aggDate > record.recordDate ? aggObject.aggDate : record.recordDate,
                    aggWeight: (parseInt(record.weight) + aggObject.aggWeight) / 2,
                };
                return newAggObject;
            },
            aggObject,
        );
        MeasurementsAggregadet.update(
            { _id: aggRecord._id },
            {
                $set: {
                    recordDate: aggObject.aggDate,
                    weight: aggObject.aggWeight,
                },
            },
        );
    } else {
        MeasurementsAggregadet.insert({ label, recordDate, weight });
    }
};
Measurements.after.insert(function(userId, doc) {
    const { label, recordDate, weight } = doc;
    updateAggregadet({
        label,
        recordDate,
        weight,
    });
});
Measurements.after.update(function(userId, doc, fieldNames, modifier, options) {
    const { label, recordDate, weight } = doc;
    updateAggregadet({
        label,
        recordDate,
        weight,
    });
});

export default Measurements;
