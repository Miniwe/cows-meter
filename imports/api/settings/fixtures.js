import times from 'lodash/times';
import Settings from './collection';

const makeFakeData = () => {
  const item = {
    code: 'TCP_PORT',
    label: 'TCP Port',
    value: '57787'
  };
  return item;
};

const fixtures = (count = 1) => {
  if (count === 0) {
    Settings.remove({});
  } else if (!Settings.find().count()) {
    times(count, () => {
      Settings.insert(makeFakeData(), { selector: { type: 'main' } });
    });
  }
};

export default fixtures;
