// import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Settings from './collection';
import rateLimit from '../../modules/rate-limit.js';
import MainSchema from './schemas/main';

export const upsertSetting = new ValidatedMethod({
  name: 'settings.upsert',
  // validate: MainSchema.validator(),
  validate(data) {
    check(data, Object);
  },
  run(setting) {
    return Settings.upsert({
      _id: setting._id
    }, { $set: setting }); // , { selector: { type: 'main' } }
  }
});

export const removeSetting = new ValidatedMethod({
  name: 'settings.remove',
  validate(id) {
    check(id, String);
  },
  run(id) {
    if (Settings.remove(id)) {
      return 'record_removed';
    }
    else {
      throw new Meteor.Error('settings', 'record_remove_error');
    }
  }
});

rateLimit({
  methods: [
    upsertSetting, removeSetting
  ],
  limit: 5,
  timeRange: 1000
});
