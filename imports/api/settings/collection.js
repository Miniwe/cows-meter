import { Mongo } from 'meteor/mongo';
import extend from 'lodash/extend';
import MainSchema from './schemas/main';
// import ListSchema from './schemas/list';
// import ViewSchema from './schemas/view';

const Settings = new Mongo.Collection('Settings');

extend(Settings, {
  ids() {
    return this.find({}, { fields: { _id: 1 } }).fetch().map((item) => item._id);
  },
  getSchema(name) {
    let schema;
    switch (name) {
      case 'list':
        schema = ListSchema;
        break;
      case 'view':
        schema = ViewSchema;
        break;
      default:
        schema = MainSchema;
    }
    return schema;
  },
  list() {
    return this.find();
  },
});

Settings.allow({
  insert : () => false,
  update : () => false,
  remove : () => false,
});

Settings.deny({
  insert : () => true,
  update : () => true,
  remove : () => true,
});

Settings.attachSchema(MainSchema);
// Settings.attachSchema(MainSchema, { selector: { type: 'main' } });
// Settings.attachSchema(ListSchema, { selector: { type: 'list' } });
// Settings.attachSchema(ViewSchema, { selector: { type: 'view' } });

export default Settings;
