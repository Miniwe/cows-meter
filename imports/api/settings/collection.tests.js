/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { assert } from 'meteor/practicalmeteor:chai';
import Settings from './collection';

describe('Settings collection', function () {
  it('registers the collection with Mongo properly', function () {
    assert.equal(typeof Settings, 'object');
  });
});
