import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Settings from '../collection';

Meteor.publish('settings.list', (query = {}, params = {}) => {
  check(query, Object);
  check(params, Object);
  return Settings.find(query, params);
});

Meteor.publish('settings.single', (id) => {
  check(id, Match.Maybe(String, null));
  return Settings.find(id);
});
