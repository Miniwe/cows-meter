/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Factory } from 'meteor/dburles:factory';
import Settings from './collection';
import { upsertSetting, removeSetting } from './methods';

describe('Settings methods', function () {
  beforeEach(function () {
    if (Meteor.isServer) {
      resetDatabase();
    }
  });

  it('inserts a setting into the Settings collection', function () {
    upsertSetting.call({title: 'You can\'t arrest me, I\'m the Cake Boss!', body: 'They went nuts!'});

    const getSetting = Settings.findOne({title: 'You can\'t arrest me, I\'m the Cake Boss!'});
    assert.equal(getSetting.body, 'They went nuts!');
  });

  it('updates a setting in the Settings collection', function () {
    const { _id } = Factory.create('setting');

    upsertSetting.call({ _id, title: 'You can\'t arrest me, I\'m the Cake Boss!', body: 'They went nuts!'});

    const getSetting = Settings.findOne(_id);
    assert.equal(getSetting.title, 'You can\'t arrest me, I\'m the Cake Boss!');
  });

  it('removes a setting from the Settings collection', function () {
    const { _id } = Factory.create('setting');
    removeSetting.call({ _id });
    const getSetting = Settings.findOne(_id);
    assert.equal(getSetting, undefined);
  });
});
