import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import Collection from './collection';

const settingsOptions = {
  name: 'Settings',
  collection: Collection,
  columns: [
    { data: 'code', title: 'Code' },
    { data: 'label', title: 'Label' },
    { data: 'value', title: 'Value' },
    {
      tmpl: Meteor.isClient && Template.settingsCell
    }
  ],
  sDom: 'ft<"row"<"col-3"l><"col-2"i><"col-7"p>>',
  responsive: true,
  // dataSrc: 'group',
  autoWidth: !(Meteor.isClient && window.screen.availWidth < 480),
  changeSelector(selector) {
    return selector;
  }
};

const sTabular = new Tabular.Table(settingsOptions);
