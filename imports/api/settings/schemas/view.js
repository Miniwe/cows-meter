import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

const ViewSchema = new SimpleSchema({
  _id: {
    type: String,
    label: 'Id'
  },
  label: {
    type: String,
    label: 'Label',
  },
  value: {
    type: String,
    label: 'Value',
  }
});

export default ViewSchema;
