import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions([ 'autoform' ]);

const MainSchema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
    label: 'Id',
    autoform: {
      afFieldInput: {
        type: "hidden"
      },
      afFormGroup: {
        label: false
      }
    }
  },
  code: {
    type: String,
    label: 'code'
  },
  label: {
    type: String,
    label: 'Label',
  },
  value: {
    type: String,
    label: 'Value',
  }
});

export default MainSchema;
