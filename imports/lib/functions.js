import { TAPi18n } from 'meteor/tap:i18n';
import { Bert } from 'meteor/themeteorchef:bert';

const handleErr = (error) => {
  if (error) {
    Bert.alert(TAPi18n.__(error.reason), 'danger', 'growl-bottom-left');
  }
};

const handleResponse = (error, result) => {
  if (error) {
    handleErr(error);
  } else {
    Bert.alert(TAPi18n.__(result), 'success', 'growl-bottom-left');
  }
};

export { handleErr, handleResponse };
