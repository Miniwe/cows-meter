import { TAPi18n } from 'meteor/tap:i18n';
import { Router } from 'meteor/iron:router';
import '../ui/layouts/mainLayout.js';
import '../ui/layouts/guestLayout.js';
import '../ui/pages/homePage.js';
import '../ui/pages/notFoundPage.js';
import './routes/auth';
import './routes/measurements';
import './routes/users';
import './routes/admin';
import './routes/animals';

Router.configure({
  title: TAPi18n.__('Site Title'),
  layoutTemplate: 'mainLayout',
  noRoutesTemplate: 'notFoundPage',
});


Router.onBeforeAction(function () {
  if (!Meteor.user()) {
    this.layout('guestLayout');
    Router.go('login');
  } else {
    TAPi18n.setLanguage(Meteor.user().language);
  }
  this.next();
});

Router.map(function () {
  this.route('root', {
    title: TAPi18n.__('Site Title'),
    path: '/',
    template: 'homePage',
  });
});


Router.route('/(.*)', function() {
  this.layout('guestLayout');
  this.render('notFoundPage');
});
