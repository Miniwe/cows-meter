import { AccountsTemplates } from 'meteor/useraccounts:core';

AccountsTemplates.configure({
  onSubmitHook: (error, state) => {
    if (!error && state === 'signIn') {
      // login successful, route to index
      Router.go('root');
    }
  },
  onLogoutHook: (error, state) => {
    Router.go('login');
  },
});

AccountsTemplates.addFields([
  {
    _id: "username",
    type: "text",
    displayName: "Username",
    required: true,
    minLength: 6
  },
]);
if (Meteor.isServer) {
//   Accounts.config({
//     sendVerificationEmail: false,
//     forbidClientAccountCreation: false,
//     restrictCreationByEmailDomain: ''
//   });

  Accounts.onCreateUser((options, user) => {
    user.roles = [];
    return user;
  });
}
