import { TAPi18n } from 'meteor/tap:i18n';
import Measurements from '../../api/measurements/collection';
import '../../ui/pages/measurementsPage.js';

Router.map(function () {
  this.route('measurements', {
    title: TAPi18n.__('Measurements'),
    path: '/measurements',
    template: 'measurementsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'mList': { to: 'pageContent' }
    },
    waitOn() {
      return [
        // Meteor.subscribe('measurements.list')
      ];
    }
  });
  this.route('measurements.new', {
    title: TAPi18n.__('Measurements New'),
    path: '/measurements/new',
    template: 'measurementsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'mForm': { to: 'pageContent' }
    }
  });
  this.route('measurements.edit', {
    title: TAPi18n.__('Measurements Edit'),
    path: '/measurements/:_id/edit',
    template: 'measurementsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'mForm': { to: 'pageContent' }
    },
    waitOn() {
      return [
        Meteor.subscribe('measurements.single', this.params._id),
      ];
    },
    data() {
      return Measurements.findOne({ _id: this.params._id });
    },

  });
});
