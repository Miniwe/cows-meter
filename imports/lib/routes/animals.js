import '../../ui/pages/animalsPage';
import '../../ui/animals/animalsList';

Router.map(function () {
  this.route('animals', {
    title: 'Animals List',
    path: '/animals',
    template: 'animalsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'animalsList': { to: 'pageContent' }
    },
    waitOn() {
      return [
        // Meteor.subscribe('measurements.list')
      ];
    }
  });

  this.route('animals.single', {
    title: 'Animal',
    path: '/animals/:label',
    template: 'animalsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'animalView': { to: 'pageContent' }
    },
    waitOn() {
      return [
        Meteor.subscribe('measurements.list', {label: this.params.label})
      ];
    }
  });
});
