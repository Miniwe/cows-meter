import { TAPi18n } from 'meteor/tap:i18n';
import Users from '../../api/users/collection';
import '../../ui/pages/usersPage.js';
import '../../ui/pages/profilePage.js';

Router.map(function () {
  this.route('profile', {
    title: TAPi18n.__('Profile'),
    path: '/profile',
    template: 'profilePage',
    layoutTemplate: 'mainLayout',
    onBeforeAction() {
      if (!Meteor.user()) {
        Router.go('root');
      } else {
        this.next();
      }
    },
    data() {
      return Meteor.user();
    },
    waitOn() {
      return [
        Meteor.subscribe('users.single', Meteor.userId())
      ];
    },
  });
  this.route('users', {
    title: TAPi18n.__('Users'),
    path: '/users',
    template: 'usersPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'uList': { to: 'pageContent' }
    },
    waitOn() {
      return [
        Meteor.subscribe('users.list')
      ];
    }
  });
  this.route('users.edit', {
    title: TAPi18n.__('User Edit'),
    path: '/users/:_id/edit',
    template: 'usersPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'uForm': { to: 'pageContent' }
    },
    waitOn() {
      return [
        Meteor.subscribe('users.single', this.params._id),
      ];
    },
    data() {
      return Users.findOne(this.params._id);
    },

  });
});

