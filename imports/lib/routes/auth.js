Router.map(function () {
  this.route('login', {
    path: '/login',
    template: 'loginPage',
    layoutTemplate: 'guestLayout',
    onBeforeAction() {
      if (Meteor.user()) {
        this.layout('mainLayout');
        Router.go('root');
      }
      this.next();
    },
  });
  this.route('logout', {
    path: '/logout',
    onBeforeAction() {
      if (Meteor.user()) {
        Meteor.logout();
        Router.go('login');
      }
      this.next();
    },
  });
  this.route('signUp', {
    path: '/signUp',
    template: 'signUpPage',
    layoutTemplate: 'guestLayout',
  });
  this.route('resetPassword', {
    path: '/reset-password',
    template: 'resetPasswordPage',
    layoutTemplate: 'guestLayout',
  });
});
