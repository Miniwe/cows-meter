import Settings from '../../api/settings/collection';
import '../../ui/pages/settingsPage';
import '../../ui/pages/logsPage';

Router.map(function () {
  this.route('logs', {
    title: 'Logs List',
    path: '/logs',
    template: 'logsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'logsList': { to: 'pageContent' }
    },
    waitOn() {
      return [
        // Meteor.subscribe('logs.list')
      ];
    }
  });
  this.route('settings', {
    title: 'Settings List',
    path: '/settings',
    template: 'settingsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'sList': { to: 'pageContent' }
    },
    waitOn() {
      return [
        // Meteor.subscribe('settings.list')
      ];
    }
  });
  this.route('settings.new', {
    title: 'Settings New',
    path: '/settings/new',
    template: 'settingsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'sForm': { to: 'pageContent' }
    }
  });
  this.route('settings.edit', {
    title: 'Settings Edit',
    path: '/settings/:_id/edit',
    template: 'settingsPage',
    layoutTemplate: 'mainLayout',
    yieldTemplates: {
      'sForm': { to: 'pageContent' }
    },
    waitOn() {
      return [
        Meteor.subscribe('settings.single', this.params._id),
      ];
    },
    data() {
      return Settings.findOne({ _id: this.params._id });
    },

  });
});
