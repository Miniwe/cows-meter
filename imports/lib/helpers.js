import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

const formatWeight = (val) => {
  return (parseInt(val, 10) * 0.1).toFixed(1);
};

const inc = (val) => {
  return 1 + val;
};

if (Meteor.isClient) {
  Template.registerHelper('formatWeight', formatWeight);
  Template.registerHelper('inc', inc);
  Template.registerHelper('loopCount', (count) => {
    const countArr = [];
    for (let i = 0; i < count; i++) {
      countArr.push({});
    }
    return countArr;
  });
}

export {
  formatWeight,
  inc
};
