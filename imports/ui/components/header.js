import './header.html';
import './header.scss';

Template.header.helpers({});

Template.header.events({
  'click a.logout'(event, template) {
    event.preventDefault();
    Meteor.logout();
  },
});
