import './sidebar.html';
import './sidebar.scss';

Template.sidebar.onRendered(function() {
  $('#sidebarCollapse').on('click', function() {
    $('#sidebar, #content').toggleClass('active');
    $('.collapse.in').toggleClass('in');
    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  });
});

Template.sidebar.events({
  'click a.logout'(event, template) {
    event.preventDefault();
    Meteor.logout();
  },
  'click .nav-link'(event, template) {
    template.$('li').removeClass('active');
    $(event.currentTarget).parents('li').addClass('active');
  },
});
