import { AutoForm } from 'meteor/aldeed:autoform';
import Settings from '../../api/settings/collection';
import MainSchema from '../../api/settings/schemas/main';
import './form.html';

const onRendered = () => {};
const onCreated = () => {
  AutoForm.addHooks('EditSettingsRecord', {
    after: {
      method(error, result) {
        if (!error) {
          if (result.insertedId) {
            Router.go('settings.edit', { _id: result.insertedId });
          }
        }
      },
    },
  });
};

Template.sForm.onCreated(onCreated);

Template.sForm.onRendered(onRendered);

Template.sForm.helpers({
  Settings: () => Settings,
  schema: () => MainSchema,
  doc: () => Template.instance().data
});

Template.sForm.events({
  'submit form'(event, template) {
  },
});
