import isEmpty from 'lodash/isEmpty';
import {moment} from 'meteor/momentjs:moment';
import isDate from 'lodash/isDate';
import '../measurements/labels';
import './animalsTable';
import './animalsList.html';

Template
    .animalsList
    .onDestroyed(function () {});

Template
    .animalsList
    .onCreated(function () {
        this.limit = new ReactiveVar(10);
        Session.set('animals.query', {});
        Session.set('animals.params', {
            page: 1,
            limit: 10
        });

        this.count = new ReactiveVar(0);
        this.animals = new ReactiveVar([]);
        this.totals = new ReactiveVar({});
        this.progress = new ReactiveVar(false);

        this.getAnimals = () => {
            this
                .progress
                .set(true);
            Meteor.call('animals.list', {
                query: Session.get('animals.query'),
                params: Session.get('animals.params')
            }, (error, result) => {
                this
                    .totals
                    .set(result.totals);
                this
                    .animals
                    .set(result.table);
                this
                    .count
                    .set(result.count);
                this
                    .progress
                    .set(false);
            });
        };

        this.autorun(() => {
            const limit = Session
                .get('animals.params')
                .limit;
            const page = Session
                .get('animals.params')
                .page;
            this.getAnimals();
        });
    });

Template
    .animalsList
    .onRendered(function () {});

Template
    .animalsList
    .events({
        'change #show_length' (event) {
            Session.set('animals.params', {
                page: 1,
                limit: parseInt($(event.target).val(), 10)
            });
        },
        'change #latest' (event, templateInstance) {
            const selector = Session.get('animals.query');
            if ($(event.currentTarget).is(':checked')) {
                selector.latest = true;
            } else {
                delete selector.latest;
            }
            Session.set('animals.query', selector);
            templateInstance.getAnimals();
        },
        'change #labels' (event, templateInstance) {
            const value = new $(event.currentTarget).val();
            const selector = Session.get('animals.query');
            if (!isEmpty(value)) {
                selector.label = value;
            } else {
                if (selector.label) {
                    delete selector.label;
                }
                if (isEmpty(selector.label)) {
                    delete selector.label;
                }
            }
            Session.set('animals.query', selector);
            templateInstance.getAnimals();
        },
        'change #start' (event, templateInstance) {
            const value = new Date($(event.currentTarget).val());
            const selector = Session.get('animals.query');
            if (isDate(value)) {
                if (!selector.recordDate) {
                    selector.recordDate = {};
                }
                selector.recordDate.$gt = moment
                    .utc(new Date(value))
                    .startOf('day')
                    .toDate();
            } else {
                if (selector.recordDate && selector.recordDate.$gt) {
                    delete selector.recordDate.$gt;
                }
                if (isEmpty(selector.recordDate)) {
                    delete selector.recordDate;
                }
            }
            Session.set('animals.query', selector);
            templateInstance.getAnimals();
        },
        'change #end' (event, templateInstance) {
            const value = new Date($(event.currentTarget).val());
            const selector = Session.get('animals.query');
            if (isDate(value)) {
                if (!selector.recordDate) {
                    selector.recordDate = {};
                }
                selector.recordDate.$lt = moment
                    .utc(new Date(value))
                    .startOf('day')
                    .toDate();
            } else {
                if (selector.recordDate && selector.recordDate.$де) {
                    delete selector.recordDate.$де;
                }
                if (isEmpty(selector.recordDate)) {
                    delete selector.recordDate;
                }
            }
            Session.set('animals.query', selector);
            templateInstance.getAnimals();
        }
    });

Template
    .loop
    .events({
        'click .paginate_button a' (event) {
            const newParams = {
                ...Session.get('animals.params'),
                page: $(event.currentTarget).data('id')
            };
            Session.set('animals.params', newParams);
        }
    });
Template
    .loop
    .helpers({
        active: (index) => (Session.get('animals.params').page - 1 == index)
            ? 'active'
            : ''
    });

Template
    .animalsList
    .helpers({
        progress: () => Template
            .instance()
            .progress
            .get(),
        animals: () => Template
            .instance()
            .animals
            .get(),
        totals: () => Template
            .instance()
            .totals
            .get(),
        count: () => Template
            .instance()
            .count
            .get(),
        from: () => Session
            .get('animals.params')
            .limit * (Session.get('animals.params').page - 1),
        to: () => Session
            .get('animals.params')
            .limit * Session
            .get('animals.params')
            .page,
        pageCount: () => Template
            .instance()
            .count
            .get() / Session
            .get('animals.params')
            .limit,
        currentPage: () => Session
            .get('animals.params')
            .page
    });
