import Chart from 'chart.js/dist/Chart.min.js';
import moment from 'moment';
import map from 'lodash/map';
import sortBy from 'lodash/sortBy';
import Measurements from '../../api/measurements/collection';
import './animalsTable';
import './animalView.html';

Template.animalView.onRendered(function() {
  const ctx = document.getElementById("myChart").getContext('2d');
  const list = sortBy(Measurements.find({ label: Router.current().params.label }).fetch(), ({recordDate}) => recordDate);
  const labels = map(list, ({ recordDate }) => moment(recordDate).calendar());
  const data = map(list, ({ weight }) => (parseInt(weight, 10) * 0.1).toFixed(1));

  const myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels,
      datasets: [ {
        label: 'weight',
        backgroundColor: '#3b9ff3',
        borderColor: '#3b9ff3',
        data: data,
        borderWidth: 2,
        fill: false
      } ]
    },
    options: {
      legend: false,
      layout: {
        padding: {
          left: 5,
          right: 5,
          top: 5,
          bottom: 5
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });

});

Template.animalView.helpers({
  selector() {
    return { label: Router.current().params.label };
  }
});
