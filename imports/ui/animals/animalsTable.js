import {moment} from 'meteor/momentjs:moment';
import './animalsTable.html';

Template
    .animalsTable
    .events({
        'click tr.row-link > td' (event) {
            const animalLabel = $(event.target)
                .parents('tr.row-link')
                .data('id');
            Router.go('animals.single', {label: animalLabel});
        }
    });

Template
    .animalsTable
    .helpers({
        activeCell(date) {
            const selectedDate = $('#end').val();

            const actualDate = moment.isDate(selectedDate)
                ? moment(new Date(selectedDate)).format('MMMM Do YYYY, h:mm')
                : moment(new Date()).format('MMMM Do YYYY, h:mm');
            return date
                .split(',')
                .shift() == actualDate
                .split(',')
                .shift()
                ? 'active'
                : '';
        }
    });
