import './homePage.html';
import { handleErr } from '../../lib/functions';

Template.homePage.onCreated(function() {
    this.animalsCount = new ReactiveVar(0);
    this.usersCount = new ReactiveVar(0);
    this.measurementsCount = new ReactiveVar(0);
    this.animalsDate = new ReactiveVar(new Date());
    this.usersDate = new ReactiveVar(new Date());
    this.measurementsDate = new ReactiveVar(new Date());
  });

  Template.homePage.onRendered(function() {
    Meteor.call('global.getStatistics', (err, res) => {
      if (err) {
        handleErr(err);
      } else {
        this.animalsCount.set(res.animalsCount);
        this.usersCount.set(res.usersCount);
        this.measurementsCount.set(res.measurementsCount);
        this.animalsDate.set(res.animalsDate);
        this.usersDate.set(res.usersDate);
        this.measurementsDate.set(res.measurementsDate);
      }
    })
});

Template.homePage.helpers({
    animalsCount: () => Template.instance().animalsCount.get(),
    usersCount: () => Template.instance().usersCount.get(),
    measurementsCount: () => Template.instance().measurementsCount.get(),
    animalsDate: () => Template.instance().animalsDate.get(),
    usersDate: () => Template.instance().usersDate.get(),
    measurementsDate: () => Template.instance().measurementsDate.get(),
});
