import { handleResponse } from '../../lib/functions.js';
import './cell.html';

Template.usersCell.events({
  'click button.remove'(event, template) {
    event.preventDefault();
    if (confirm('Remove Record')) {
      const { _id } = template.data;
      Meteor.call('users.remove', _id, handleResponse);
    }
  }
});
