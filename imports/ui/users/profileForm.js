import Users from '../../api/users/collection';
import ProfileSchema from '../../api/users/schemas/profile';
import {handleErr} from '../../lib/functions';
import './profileForm.html';

Template
    .profileForm
    .helpers({
        Users: () => Users,
        schema: () => ProfileSchema,
        doc: () => Template
            .instance()
            .data
    });

Template
    .profileForm
    .onCreated(function () {
        AutoForm.addHooks('EditProfileRecord', {
            after: {
                method(error, result) {
                    if (!error) {
                        Router.go('root');
                    } else {
                        handleErr(error);
                    }

                }
            }
        });
    });
