import {AutoForm} from 'meteor/aldeed:autoform';
// import Users from '../../api/users/collection';
import MainSchema from '../../api/users/schemas/main';
import {handleErr} from '../../lib/functions';
import './form.html';

const onRendered = () => {};
const onCreated = () => {
    AutoForm.addHooks('EditRecord', {
        after: {
            method(error, result) {
                if (!error) {
                    Router.go('users');
                    //   if (result.insertedId) {     Router.go('users.edit', { _id:
                    // result.insertedId });   }
                } else {
                    handleErr(error);
                }
            }
        }
    });
};

Template
    .uForm
    .onCreated(onCreated);

Template
    .uForm
    .onRendered(onRendered);

Template
    .uForm
    .helpers({
        schema: () => MainSchema,
        doc: () => Template
            .instance()
            .data
    });
