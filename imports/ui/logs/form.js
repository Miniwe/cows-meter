import { AutoForm } from 'meteor/aldeed:autoform';
import Logs from '../../api/logs/collection';
import MainSchema from '../../api/logs/schemas/main';
import './form.html';

const onRendered = () => {};
const onCreated = () => {
  AutoForm.addHooks('EditRecord', {
    after: {
      method(error, result) {
        if (!error) {
          if (result.insertedId) {
            Router.go('logs.edit', { _id: result.insertedId });
          }
        }
      },
    },
  });
};

Template.logsForm.onCreated(onCreated);

Template.logsForm.onRendered(onRendered);

Template.logsForm.helpers({
  Logs: () => Logs,
  schema: () => MainSchema,
  doc: () => Template.instance().data
});

Template.logsForm.events({
  'submit form'(event, template) {
  },
});
