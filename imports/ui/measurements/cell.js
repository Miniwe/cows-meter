import { handleResponse } from "../../lib/functions.js";
import './cell.html';

Template.measurementCell.events({
  'click button.remove'(event, template) {
    event.preventDefault();
    if (confirm('Remove Record')) {
      const { _id } = template.data;
      Meteor.call('measurements.remove', _id, handleResponse);
    }
  }
});
