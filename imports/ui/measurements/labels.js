import { TAPi18n } from 'meteor/tap:i18n';
import Measurements from '../../api/measurements/collection';
import './labels.html';
import './labels.scss';

Template.labelsSelect.onRendered(function() {
  const $select = this.$('select').select2({
    placeholder: TAPi18n.__('Search Label'),
    allowClear: true,
    minimumInputLength: 1,
    theme: "bootstrap",
    ajax: {
      data(params) {
        return params;
      },
      transport(params, success, failure) {
        Meteor.call('measurements.labels', params.data, (err, results) => {
          if (!err) {
            success(results);
          } else {
            failure(err);
          }
        });
      },
      processResults(data, params) {
        return {
          results: $.map(data, (item) => {
            return {
              text: item,
              id: item,
            };
          }),
        };
      }
    },
  });
});
