import {Session} from 'meteor/session';
import {moment} from 'meteor/momentjs:moment';
import isEmpty from 'lodash/isEmpty';
import isDate from 'lodash/isDate';
import './labels';
import './list.html';
import './cell';
import '../../api/measurements/tabular';

Template
    .mList
    .onRendered(function () {
        Session.set('measurements.selector', {});
    });

Template
    .mList
    .onCreated(function () {
        this.aggregated = new ReactiveVar(false);
    });

Template
    .mList
    .helpers({
        selector() {
            return Session.get('measurements.selector');
        },
        aggregated() {
            return Template
                .instance()
                .aggregated
                .get();
        }
    });

Template
    .mList
    .events({
        'change #aggregate' (event, templateInstance) {
            templateInstance
                .aggregated
                .set($(event.currentTarget).is(':checked'));
        },
        'change #labels' (event) {
            const value = new $(event.currentTarget).val();
            const selector = Session.get('measurements.selector');
            if (!isEmpty(value)) {
                selector.label = value;
            } else {
                if (selector.label) {
                    delete selector.label;
                }
                if (isEmpty(selector.label)) {
                    delete selector.label;
                }
            }
            Session.set('measurements.selector', selector);
            $('.dataTable')
                .dataTable()
                .fnDraw();
        },
        'change #start' (event) {
            const value = new Date($(event.currentTarget).val());
            const selector = Session.get('measurements.selector');
            if (isDate(value)) {
                if (!selector.recordDate) {
                    selector.recordDate = {};
                }
                selector.recordDate.$gt = moment
                    .utc(new Date(value))
                    .startOf('day')
                    .toDate();
            } else {
                if (selector.recordDate && selector.recordDate.$gt) {
                    delete selector.recordDate.$gt;
                }
                if (isEmpty(selector.recordDate)) {
                    delete selector.recordDate;
                }
            }
            Session.set('measurements.selector', selector);
            $('.dataTable')
                .dataTable()
                .fnDraw();
        },
        'change #end' (event) {
            const value = new Date($(event.currentTarget).val());
            const selector = Session.get('measurements.selector');
            if (isDate(value)) {
                if (!selector.recordDate) {
                    selector.recordDate = {};
                }
                selector.recordDate.$lt = moment
                    .utc(new Date(value))
                    .endOf('day')
                    .toDate();
                //   selector.recordDate.$lt = value;
            } else {
                if (selector.recordDate && selector.recordDate.$lt) {
                    delete selector.recordDate.$lt;
                }
                if (isEmpty(selector.recordDate)) {
                    delete selector.recordDate;
                }
            }
            Session.set('measurements.selector', selector);
            $('.dataTable')
                .dataTable()
                .fnDraw();
        },
        // 'keyup #min'(event) {   const value = $(event.currentTarget).val();   const
        // selector = Session.get('measurements.selector');   if (value > 0) {     if
        // (!selector.weight) {       selector.weight = {};     }
        // selector.weight.$gt = parseInt(value, 10);   } else {     if (selector.weight
        // && selector.weight.$gt) {       delete selector.weight.$gt;     }     if
        // (isEmpty(selector.weight)) {       delete selector.weight;     }   }
        // Session.set('measurements.selector', selector);
        // $('.dataTable').dataTable().fnDraw(); }, 'keyup #max'(event) {   const value
        // = $(event.currentTarget).val();   const selector =
        // Session.get('measurements.selector');   if (value > 0) {     if
        // (!selector.weight) {       selector.weight = {};     }
        // selector.weight.$lt = parseInt(value, 10);   } else {     if (selector.weight
        // && selector.weight.$lt) {       delete selector.weight.$lt;     }     if
        // (isEmpty(selector.weight)) {       delete selector.weight;     }   }
        // Session.set('measurements.selector', selector);
        // $('.dataTable').dataTable().fnDraw(); },
    });
