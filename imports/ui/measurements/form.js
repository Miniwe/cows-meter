import {AutoForm} from 'meteor/aldeed:autoform';
import Measurements from '../../api/measurements/collection';
import MainSchema from '../../api/measurements/schemas/main';
import './form.html';
import {handleErr} from '../../lib/functions';

const onRendered = () => {};
const onCreated = () => {
    AutoForm.addHooks('EditRecord', {
        after: {
            method(error, result) {
                if (!error) {
                    //   if (result.insertedId) {     Router.go('measurements.edit', { _id:
                    // result.insertedId }); }
                    Router.go('measurements');
                } else {
                    handleErr(error);
                }
            }
        }
    });
};

Template
    .mForm
    .onCreated(onCreated);

Template
    .mForm
    .onRendered(onRendered);

Template
    .mForm
    .helpers({
        Measurements: () => Measurements,
        schema: () => MainSchema,
        doc: () => Template
            .instance()
            .data
    });

Template
    .mForm
    .events({
        'submit form' (event, template) {}
    });
